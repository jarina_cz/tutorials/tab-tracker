# Tab tracker

A simple application to learn Express.js + Vue + Axios + Vuex + Vuetify and more

## Get it running

### Front End

```
cd client
npm install
npm run dev
```

### Back End
Open new terminal window/tab

```
cd server
npm install
npm start
```
